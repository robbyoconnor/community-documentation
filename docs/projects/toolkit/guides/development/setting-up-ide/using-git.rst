Using Git
=========

You need to have git installed and set up on your computer to perform the next steps of this manual. Git is a free and open source distributed version control system. You can refer to the official `documentation <https://git-scm.com/>`_ for installation instructions.

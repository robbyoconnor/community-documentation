
Running the Project
===================

IntelliJ IDEA is an IDE for Java, but it also understands and provides intelligent coding assistance for a variety of other languages like SQL, HTML, Javascript and so on. IntelliJ IDEA is used here because of its integrated version control systems and requirement of no additional plugins to equip it according to the project. IntelliJ IDEA can be used in our project to build the cloned repository. A basic version of IntelliJ IDEA is available for free on their website. Refer to `the guide <../../development/index.html#setting-up-your-integerated-development-environment-ide>`_ for the installation and cloning instructions. After downloading the application and opening it, an image as shown below appears on the screen.

.. image:: images/open-project.png
   :alt: open project


On the left, you can see the list of Maeven projects. Select ``Skip Tests`` mode by clicking the icon with a lightning.
It cancels the test option for all the dropdowns in the Librehealth toolkit.
This option is selected mainly to help the user by not running optional verification tests each time the module is built.
Under ``LibreHealth Toolkit`` chose ``Install`` as shown in the image below.

.. image:: images/skip-tests-button.png
   :alt: skip tests button


If you have not specified an SDK, you will see a warning inviting you to do it. Click on the link in the warning.

.. image:: images/no-sdk-error.png
   :alt: no sdk error


You may need to install an `SDK <https://openjdk.java.net/install/>`_. Ensure that you are using Java 1.8. Point to the SDK location on your system and confirm.

.. image:: images/setting-config.png
   :alt: setting config


After clicking ``Install``\ , IntelliJ will perform installation on the project.

.. image:: images/installation-process.png
   :alt: installation process


Install the openmrs.api by selecting the ``install`` option from ``Lifecycle`` in ``openmrs.api`` field. Double-click to perform the action.

.. image:: images/installing-open-mrs-api.png
   :alt: installing open mrs api


To run the application locally, select ``openmrs-webapp -> Plugins -> jetty -> jetty:deploy-war``. This will help in running the war program and deploying it on local system using the built-in jetty server.

.. image:: images/jetty-deploy-war.png
   :alt: deployment


To run it on the local system, double-click ``jetty:run``. It will start a jetty server.

.. image:: images/jetty-run.png
   :alt: running


Open your browser and type ``localhost:8080/openmrs`` in the address bar to start LH Toolkit installation process locally.

.. image:: images/installation-wizard.png
   :alt: installation-wizard

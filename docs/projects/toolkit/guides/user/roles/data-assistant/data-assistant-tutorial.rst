
Data Assistant: Managing Patients
=================================

Introduction
------------

The **Data Assistant** is one of the roles in LibreHealth Toolkit (LH Toolkit). This document explains how a **Data Assistant** can create new patients, edit patient information and use the ``Dictionary``. The full list of permissions for a **Data Assistant** can be found in the following table:

.. list-table::
   :header-rows: 1

   * - Manage
     - Delete
     - View
     - Add
     - Edit
     - Get
   * - Programs
     - Relationships
     - Demographics Section
     - People
     - People
     - Forms
   * -
     -
     - Forms Section
     - Patients
     - Patients
     - Concepts
   * -
     -
     - Overview Section
     - Relationships
     - Relationships
     - Encounter Roles
   * -
     -
     - Patient Actions
     -
     -
     - Encounters
   * -
     -
     - Encounters Section
     -
     -
     - Observations
   * -
     -
     - Concepts
     -
     -
     - Patient Identifiers
   * -
     -
     - Encounters
     -
     -
     - Patients
   * -
     -
     - Forms
     -
     -
     - People
   * -
     -
     - Navigation Menu
     -
     -
     - Providers
   * -
     -
     - Observations
     -
     -
     - Progtams
   * -
     -
     - Patient Identifiers
     -
     -
     - Users
   * -
     -
     - Patients
     -
     -
     - Visits
   * -
     -
     - People
     -
     -
     -
   * -
     -
     - Programs
     -
     -
     -
   * -
     -
     - Users
     -
     -
     -


Procedure
---------

Logging in as a Data Assistant
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You need to be logged in as a **Data Assistant** to proceed with this tutorial.

.. image:: images/logged-in-as-data-assistant.png
   :alt: data assistant dashboard


Available operations
^^^^^^^^^^^^^^^^^^^^

As a **Data Assistant**, you will see the dashboard with a clickable buttons ``Home``\ , ``Find/Create Patient`` and ``Dictionary``.

``Find/Create Patient`` will redirect you to the form where you can either search a patient or create a new one.


.. image:: images/find-create-patient.png
   :alt: find or create a patient


When searching a patient, autocomplete suggests possible matches. If there is a match, click on the name of this patient and you will be redirected to the corresponding patient dashboard.


.. image:: images/patient-search.png
   :alt: patient search


Here you can edit information about this patient. There are two tabs on the dashboard: ``Overview`` and ``Demographics``. You can add new relationships on the ``Overview`` tab. Click ``Add a new relationship`` to do this.


.. image:: images/patient-information.png
   :alt: patient information


Fill in the form in the popup and click ``Save``.

.. image:: images/adding-new-relationship.png
   :alt: adding new relationship


You will see this new relationship on the patient dashboard.

.. image:: images/relationship-added.png
   :alt: relationship added


Next tab is ``Demographics`` tab. Here two forms to edit patient's info are accessible.

.. image:: images/demographics.png
   :alt: demographics tab


A long form is similar to the form you fill in when creating a patient. A a short form is shown below:

.. image:: images/edit-patient.png
   :alt: editing a patient


Make the necessary changes here and save them.

If you want to create a patient, navigate to the ``Find/Create Patient`` by clicking a corresponding link on the dashboard. Fill in the lower part of the form and click ``Create Person``.


.. image:: images/creating-new-patient.png
   :alt: creating a new patient


You will be asked to enter information about this person. Click ``Save`` when you are ready.


.. image:: images/new-patient-info.png
   :alt: entering new patient info


Click ``Dictionary`` on the dashboard to download a .csv file with medical terms for reference or to search for a term.


.. image:: images/dictionary.png
   :alt: dictionary


Summary
-------

A **Data Assistant** can add patients, relationships, edit this information and use the Dictionary.

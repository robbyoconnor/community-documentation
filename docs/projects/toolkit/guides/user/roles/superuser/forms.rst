
System Developer: Forms
=======================

Introduction
------------

System Developer can manage forms, fields, field types and merge duplicate fields. These operations are described below.

Procedure
---------

Logging in as a System Developer
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You need to be logged in as a System Developer to proceed with this tutorial. Navigate to ``Administration`` tab on the main dashboard on the top of the page and locate ``Encounters`` chapter.

.. image:: images/forms.png
   :alt: forms


Available operations
^^^^^^^^^^^^^^^^^^^^

Manage Forms
~~~~~~~~~~~~

``Manage Forms`` will redirect you to the form where you can either add a form or edit an existing one.


.. image:: images/manage-forms.png
   :alt: manage forms


Click ``Add Form`` to access the form where you can fill in information about a new encounter or click on the name of the form to edit it.


.. image:: images/edit-form.png
   :alt: edit form


Once you are ready, click ``Save Form``.

Manage Fields
~~~~~~~~~~~~~

Click ``Manage Fields`` to search for available fields or add a new one.
To add a new field, click ``Add New Field``.


.. image:: images/manage-fields.png
   :alt: manage fields


Fill in the form and click ``Save``.


.. image:: images/new-field.png
   :alt: new field


Manage Field Types
~~~~~~~~~~~~~~~~~~

``Manage Field Types`` allows editing, deleting or adding field types.


.. image:: images/manage-field-types.png
   :alt: manage field types


Click ``Add Field Type`` and fill in the form to add a new field type.


.. image:: images/new-field-type.png
   :alt: new field type


Merge Duplicate Fields
~~~~~~~~~~~~~~~~~~~~~~

``Merge Duplicate Fields`` allows merging duplicate fields. Note that this operation cannot be undone.


.. image:: images/merge-duplicate-fields.png
   :alt: merge duplicate fields


Summary
-------

A System Developer can perform different operations to manage forms in the system.


System Developer: Providers
===========================

Introduction
------------

System Developer can manage provider and provider attribute types. These operations are described below.

Procedure
---------

Logging in as a System Developer
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You need to be logged in as a System Developer to proceed with this tutorial. Navigate to ``Administration`` tab on the main dashboard on the top of the page and locate ``Providers`` chapter.

.. image:: images/providers.png
   :alt: providers


Available operations
^^^^^^^^^^^^^^^^^^^^

Manage Providers
~~~~~~~~~~~~~~~~

``Manage Providers`` will redirect you to the form where you can either add a provider or edit an existing one.


.. image:: images/add-provider.png
   :alt: add a provider


Click ``Add Provider`` to access the form where you can fill in information about a new provider.


.. image:: images/save-provider.png
   :alt: save a provider


Once you are ready, click ``Save Provider``.

You can also edit information about an existing provider. Click on the provider ID in the list to access the edit form to make changes. You can also retire or delete a provider using this form.


.. image:: images/edit-provider.png
   :alt: edit a provider


Manage Provider Attribute Type
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Click ``Provider Attribute Type Management`` to see available provider attribute types or to add a new one.


.. image:: images/provider-attribute-types.png
   :alt: provider attribute types


To add a new attribute type, click ``Add Provider Attribute Type``\ , fill in and save the form.


.. image:: images/new-provider-attribute-type.png
   :alt: new provider attribute type


Summary
-------

A System Developer can perform different operations to manage providers in the system.

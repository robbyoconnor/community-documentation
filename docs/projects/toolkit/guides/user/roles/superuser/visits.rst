
System Developer: Visits
========================

Introduction
------------

System Developer can manage visit types, attributes and configure visits. These operations are described below.

Procedure
---------

Logging in as a System Developer
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You need to be logged in as a System Developer to proceed with this tutorial. Navigate to ``Administration`` tab on the main dashboard on the top of the page and locate ``Visits`` chapter.

.. image:: images/visits-chapter.png
   :alt: visits chapter


Available operations
^^^^^^^^^^^^^^^^^^^^

Manage Visit Types
~~~~~~~~~~~~~~~~~~

``Manage Visit Types`` will redirect you to the form where you can either edit an existing visit type or create a new one.


.. image:: images/create-visit-type.png
   :alt: create visit type


Click ``Add Visit Type`` to access the form where you can fill in information about a new visit type.


.. image:: images/describe-visit-type.png
   :alt: describe visit type


Once you are ready, click ``Save Visit Type``. This will redirect you to the previous page and the thew visit type will be added to the list.
You can edit it, retire or delete by clicking on this visit type and making the necessary changes in the form. If you choose to retire a visit type, it will appear crossed in the list of visit types.


.. image:: images/edit-visit-type.png
   :alt: edit visit type


Manage Visit Attribute Types
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Click ``Manage Visit Attribute Types`` to see available relationship types or add a new one.
To add a new visit attribute type, click ``Add Visit Attribute Type``.


.. image:: images/visit-attribute-types.png
   :alt: visit attribute types


Fill in information on the new visit attribute type and click ``Save Visit Attribute Type``

.. image:: images/add-visit-attribute-type.png
   :alt: add visit attribute type


You will see this new type in the list of available types and can change or delete it by clicking on it and making necessary changes.

Configure Visits
~~~~~~~~~~~~~~~~

``Configure Visits`` allows enabling or disabling visits and starting auto close visit tasks. Click ``Save`` after making the necessary changes to apply them.

.. image:: images/configure-visits.png
   :alt: configure visits


Summary
-------

A System Developer can perform different operations to manage visits in the system.

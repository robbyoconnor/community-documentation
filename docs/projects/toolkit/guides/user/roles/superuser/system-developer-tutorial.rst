
System Developer: Customizing Toolkit
=====================================

Introduction
------------

System Developer is one of the roles in LibreHealth Toolkit. It is the role of the vastest range of privileges and can perform all tasks available in the system.

Procedure
---------

Dashboard Overview
^^^^^^^^^^^^^^^^^^

To do this, you need to be logged in as a System Administrator. On the main dashboard on the top, four tabs are available: ``Home``\, ``Find/Create Patient``\, ``Dictionary`` and ``Administration``.

.. image:: images/dashboard.png
   :alt: dashboard


``Home`` leads to the welcome screen and does not have options to perform other tasks.

``Find/Create Patient`` is described in the `Data Manager tutorial <../data-manager/data-manager-tutorial.html#find-create-patient>`_ .

``Dictionary`` gives access to the dictionary of medical terms. The detailed description of this tab is `here <./users.html#dictionary>`_.

``Administration`` tab allows System Developer to perform the vast variety of tasks. They are logically grouped and each group is described in detail below.

Administration Functions
^^^^^^^^^^^^^^^^^^^^^^^^

There are 17 groups of tasks on the ``Administration`` dashboard.

``Users`` allows creating and changing users, manage privileges and alerts.

.. seealso::  For more information See the `users tutorial  <./users.html>`_ for more information.

``Patients`` allows creating and editing patients, merging patients and working with identifier types.

.. seealso:: For more information see the Data Manager `tutorial for managing patients. <../data-manager/data-manager-tutorial.html#patients>`_.

``Person`` allows creating and changing persons, setting their relationships and attribute types.

.. seealso:: For more information see the `person tutorial  <./person.html>`_ for managing persons.

``Visits`` allows performing tasks with visits; edit or add visit types and attributes and configure visits.

.. seealso:: For more information on working with Visits, see the `visits tutorial <./visits.html>`_

``Encounters`` allows adding new encounters and configuring existing ones.

.. seealso:: For more information on Encounters see the `Encounters tutorial <./encounters.html>`_

``Providers`` allows managing providers and provider attribute types. `View <./providers.html>`_

.. seealso:: For more information on Providers see the `Providers tutorial <./providers.html>`_

``Locations`` allows customizing address templates, adding and editing locations.

.. seealso:: For more information see the Data Manager `locations tutorial <../data-manager/data-manager-tutorial.html#locations>`_.

``Observations`` allows managing observations.

.. seealso:: For more information see the tutorial for `managing encounters <./encounters.html#manage-encounters>`_

``Scheduler`` allows managing scheduler.

.. seealso:: For more information see tutorial on `how to work with the scheduler <./scheduler.html>`_.

``Programs`` allows managing programs and triggered state conversions.

.. seealso:: For more information see Data Manager `tutorial for managing and working work programs. <../data-manager/data-manager-tutorial.html#programs>`_.

``Concepts`` allows viewing and managing concept drugs, classes, datatypes. sources, attribute types and so on.

.. seealso:: For more information see `tutorial for working with Concepts <./concepts.html>`_.

``Forms`` allows managing forms, fields and field types, and merge duplicate fields.

.. seealso:: For more information see `tutorial for working with forms <./forms.html>`_.

``HL7 Messages`` allows managing HL7 sources, errors, manage and migrate archives, manage queued and held messages.

.. seealso:: For more information see `tutorial for working with HL7 messages <./hl7-messages.html>`_.

``Maintenance`` allows setting system preferences and viewing system information.

``Modules`` allows managing modules and their properties.

``REST Web Services`` gives access to API documentation, settings, and tests.

``Open Web Apps Module`` allows the user to manage apps and access settings.

Summary
-------

A System Developer has the widest range of permissions or privileges and can customize LibreHealth Toolkit.

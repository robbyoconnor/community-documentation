
System Developer: HL7 Messages
==============================

Introduction
------------

System Developer can manage HL7 sources, queued messages, held messages, HL7 errors, manage and migrate archives. These operations are described below.

Procedure
---------

Logging in as a System Developer
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You need to be logged in as a System Developer to proceed with this tutorial. Navigate to ``Administration`` tab on the main dashboard on the top of the page and locate ``HL7 Messages`` chapter.

.. image:: images/hl7-messages.png
   :alt: HL7 messages


Available operations
^^^^^^^^^^^^^^^^^^^^

Manage HL7 Sources
~~~~~~~~~~~~~~~~~~

``Manage HL7 Sources`` will redirect you to the form where you can either add a new HL7 source or edit an existing one.


.. image:: images/manage-sources.png
   :alt: manage sources


Click ``Add HL7 Source`` to access the form where you can fill in information about a new source or click on the name of the existing source to edit it.


.. image:: images/hl7-source.png
   :alt: HL7 source


Manage Queued Messages, Held Messages, Errors, HL7 Archives
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Each of ``Manage Queued Messages, Held Messages, Errors, HL7 Archives`` redirects you to the similar table where you can search for respective items.
The example is shown below.


.. image:: images/hl7-table.png
   :alt: HL7 table


Migrate HL7 Archives
~~~~~~~~~~~~~~~~~~~~

``Migrate HL7 Archives`` is intended to migrate system archives.


.. image:: images/manage-field-types.png
   :alt: manage field types


Summary
-------

A System Developer can perform different operations to manage HL7 messages in the system.
